# PedidosYa Backend App

Esta aplicación fue escrita en NodeJS, Typescript y se conecta con una base de datos Redis.

## Instrucciones para ejecutar

Clonar el proyecto
```javascript
git clone https://gitlab.com/pedidos-ya/backend.git
```

Crear un archivo __.env__ en la raiz con las siguientes variables de entorno:
```javascript
APP_PORT=8070
REDIS_IP=localhost
REDIS_PORT=6379
PEYA_API_URL= ****/public/v1 // URL de pedidosYa API
PEYA_AUTH_USER= **** // Usuario para autenticar aplicación
PEYA_AUTH_PASS= ****943 // Contraseña para autenticar aplicación
```

Instalar dependencias del proyecto
```shell
yarn // or npm install
```

Levantar Redis en una __terminal aparte__

```shell
docker run --rm -p 6379:6379 redis:alpine
```

Correr el proyecto

```shell
yarn serve // or npm run serve
```

Si Todo salió bien, deberia ver algo similar a lo siguiente:
```bash
PedidosYa access token obtained
Connection stablished with redis successfully
PedidosYa REST test app started successfully on port 8070
```


## Descripción

La aplicacion expone multiples endpoints para el uso de la aplicacion web, asi como algunos para administracion.

```javascript
POST /api/users/authenticate // Autentica un usuario con la aplicación y la API de pedidosYa
GET  /api/users/account      // Retorna información sobre el usuario loggueado
POST /api/users/logout       // Destruye la sesión del usuario loggueado
GET  /api/restaurants        // Retorna una lista de restaurantes en base a coordenadas 

GET  /api/admin/status          // Retorna una lista de usuarios loggueados junto a una lista de los recursos consultados
PUT  /api/admin/cacheTime/:time // Cambia el tiempo que se mantendrá el cache de los recursos consultados a la API de pedidosYa
```


## Justificación de Stack

### NodeJs
Elejí escribir la app en Node por su escalabilidad y debido a que es capaz de manejar un gran número de conexiones simultáneas con alto rendimiento. Buscaria utilizár otra tecnología si un uso intensivo del CPU fuese requerimiento.

### Typescript
Typescript me permite escribir codigo mas limpio, mantenible, y mas rápido y fácil de leer. Baja el numero de errores, provee InteliSense y transpila a una versión de JS que corren todos los browsers. 

### Redis
No sobrecarga el heap de node. Permite guardar información por un tiempo determinado, lo cuál resuelve facilmente guardar cache de querys y soluciona el problema en el caso de que la aplicación fuese a ser distribuida en varios servidores.