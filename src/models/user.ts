import Peya from './pedidosya';

export default class User {

    static async authenticate(user: string, password: string) {
        if (!user || !password) {
            throw new TypeError("params not allowed to be empty");
        }
        
        const res = await Peya.auth(user, password);
        return res;
    }

    static async getAccountInfo(token: string){
        const res = await Peya.getAccountInfo(token);
        return res;
    }

}