import Peya from './pedidosya';

export default class Restaurant {

    static async get(token: string, options: any) {
        const res = await Peya.getRestaurants(token, options);
        return res;
    }
}