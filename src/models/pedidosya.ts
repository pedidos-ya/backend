import axios, { AxiosResponse } from "axios"
require('dotenv').config()

const http = axios.create({
    baseURL: process.env.PEYA_API_URL,
    timeout: 8000,
    headers: { 'content-type': 'application/json' }
});

export default class Peya {

    static async authApp(clientId: string, clientSecret: string) {
        const r = await http.get('tokens', { params: { clientId, clientSecret } })
        return r.data;
    }

    static async auth(userName: string, password: string): Promise<AxiosResponse> {
        const peya_token = require('../index').peya_token

        const res = await http.get('tokens', {
            params: { userName, password },
            headers: { 'Authorization': peya_token }
        })
        return res.data
    }

    static async getAccountInfo(token: string): Promise<AxiosResponse> {
        const res = await http.get('myAccount', {
            headers: { 'Authorization': token }
        })
        return res.data
    }

    static async getRestaurants(token: string, options: any) {
        const { point, max } = options;
        const res = await http.get('search/restaurants', {
            params: { point, max, country: 1 },
            headers: { 'Authorization': token }
        })
        return res.data
    }

}