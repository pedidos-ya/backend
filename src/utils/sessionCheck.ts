
const sessionCheck = () => {
    return (req, res, next) => {
        if (req.session.email) {
            next()
        }
        else {
            res.status(401).send()
        }
    }
}

export default sessionCheck