import Redis from "ioredis";

export class RedisController {
  static client;

  static async getConnection() {
    if (!RedisController.client) {
      this.client = new Redis({
        host: process.env.REDIS_IP,
        port: process.env.REDIS_PORT,
        lazyConnect: true
      });
      try {
        await this.client.connect();
      } catch (error) {
        throw error;
      }
    }
  }

  static close() {
    try {
      this.client.disconnect();
    } catch (e) {
      throw e;
    }
  }

  static async set(key: string, value: any) {
    try {
      await this.client.set(key, JSON.stringify(value));
    } catch (err) {
      throw err;
    }
  }

  static async setex(key: string, time: number, value: any) {
    try {
      await this.client.setex(key, time, JSON.stringify(value));
    } catch (err) {
      throw err;
    }
  }

  static async get(key: string) {
    try {
      return JSON.parse(await this.client.get(key));
    } catch (err) {
      throw err;
    }
  }

  static async del(keys: Array<string>): Promise<any> {
    try {
      await this.client.del(keys);
    } catch (err) {
      throw err;
    }
  }
}