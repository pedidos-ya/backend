'use strict';

const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
const morgan = require('morgan')
import Peya from './models/pedidosya'
import { RedisController } from './utils/redis'

require('dotenv').config()

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))

app.use(morgan(":method :url :status :res[content-length] - :response-time ms"))
app.use('/api/users', require('./routes/users'));
app.use('/api/restaurants', require('./routes/restaurants'));
app.use('/api/admin', require('./routes/admin'));


(async () => {
  try {
    const clientId = process.env.PEYA_AUTH_USER
    const clientSecret = process.env.PEYA_AUTH_PASS
    const appPort = process.env.APP_PORT || 8070

    if (!clientId || !clientSecret) {
      throw 'Oops :( , Missing app enviroment variables'
    }

    const appAuth = await Peya.authApp(clientId, clientSecret)
    console.clear()
    console.log("PedidosYa access token obtained")
    module.exports.peya_token = appAuth.access_token;

    RedisController.getConnection();
    RedisController.set('loggedUsers', []);
    RedisController.set('queryCache', 60);
    console.log('Connection stablished with redis successfully');

    await app.listen(appPort);
    console.log(`PedidosYa REST test app started successfully on port ${appPort}`);

  } catch (e) {
    console.error(e);
  }
})()

