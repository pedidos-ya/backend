const express = require('express')
const router = express.Router()
import { RedisController } from '../utils/redis'
import sessionCheck from '../utils/sessionCheck'
import User from '../models/user'

router.post('/authenticate', async (req, res) => {

    const { user, password } = req.body

    try {
        const auth: any = await User.authenticate(user, password)
        let loggedUsers: any[] = await RedisController.get("loggedUsers")

        req.session.email = user;
        loggedUsers.push({ email: user })
        RedisController.set('loggedUsers', loggedUsers)
        res.send(auth)
    } catch (e) {
        res.status(401).send(e)
    }

})

router.post('/logout', sessionCheck(), async (req, res) => {
    let loggedUsers: any[] = await RedisController.get('loggedUsers')
    let index: number = loggedUsers.findIndex(u => u.email == req.session.email);

    if (index > -1) {
        loggedUsers.splice(index, 1);
    }

    RedisController.set('loggedUsers', loggedUsers);
    req.session.destroy();
    res.status(200).send();
})

router.get('/account', sessionCheck(), async (req, res) => {
    try {
        const account: any = await User.getAccountInfo(req.headers.authorization)
        res.send(account)
    } catch (e) {
        res.status(401).send(e)
    }

})

module.exports = router