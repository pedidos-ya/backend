const express = require('express')
const router = express.Router()
import sessionCheck from '../utils/sessionCheck'
import Restaurant from '../models/restaurant'
import { RedisController } from '../utils/redis'

router.get('/', sessionCheck(), async (req, res) => {
    try {
        const { point, max } = req.query
        let loggedUsers: any[] = await RedisController.get('loggedUsers')
        let queryCache: number = await RedisController.get('queryCache')
        let data: any[] = await RedisController.get(`restaurants-${point}`);
        let restaurants: any[] = []

        // Checking if query already in cache
        if (data) {
            console.log("Cache found for coordinates ", point);
            restaurants = data;
        } else {
            restaurants = await Restaurant.get(req.headers.authorization, { point, max })
            RedisController.setex(`restaurants-${point}`, queryCache, restaurants);
        }

        let index: number = loggedUsers.findIndex(u => u.email == req.session.email)
        let user = loggedUsers[index]
        let query = { time: new Date(), query: `/restaurants?point=${point}&max=${max}` }

        if (!user.querys) {
            user.querys = [query]
        } else {
            user.querys.push(query)
        }

        loggedUsers[index] = user;
        RedisController.set('loggedUsers', loggedUsers);
        res.send(restaurants);
    } catch (e) {
        console.log(e);
        res.status(401).send(e)
    }
})

module.exports = router