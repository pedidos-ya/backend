const express = require('express')
const router = express.Router()
import sessionCheck from '../utils/sessionCheck'
import { RedisController } from '../utils/redis'

router.get('/status', sessionCheck(), async (req, res) => {
    try {
        const status = {
            users: await RedisController.get('loggedUsers')
        }
        res.send(status);
    } catch (e) {
        console.log(e);
        res.status(401).send(e)
    }
})

router.put('/cacheTime/:time', sessionCheck(), async (req, res) => {
    try {
        const { time } = req.params;

        if (!time) {
            throw "time is not allowed to be empty"
        }
        RedisController.set('queryCache', time);
        res.status(200).send();
    } catch (e) {
        console.log(e);
        res.status(400).send(e)
    }
})

module.exports = router